extends KinematicBody2D

var speed = 200
var velocity = Vector2.ZERO

onready var lifetime = $Lifetime
onready var audio = $AudioStreamPlayer2D

func _ready():
	lifetime.start()
	
func _process(delta):
	position += velocity * delta * speed
	var collision = move_and_collide(velocity)
	if collision:
		velocity *= 0


func _on_Area2D_body_entered(body):
	body.hurt(1)


func _on_Lifetime_timeout():
	queue_free()
