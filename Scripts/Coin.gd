extends Area2D

func _on_Coin_body_entered(body):
	body.add_score(100)
	$AudioStreamPlayer2D.play()
	$AnimatedSprite.hide()
	collision_layer = 0
	collision_mask = 0


func _on_AudioStreamPlayer2D_finished():
	queue_free()
