extends Area2D

func _on_Potion_body_entered(body):
	body.heal(1)
	queue_free()
