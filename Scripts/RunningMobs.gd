extends KinematicBody2D

export (int) var speed = 60
export (int) var health = 5
export (int) var points = 200

onready var animation = $AnimatedSprite
onready var invinTimer = $invincibilityTimer

var player = null
var player_node = null
var velocity = Vector2()
var is_hurt = false
var is_dead = false

func _process(_delta):
	velocity = Vector2.ZERO
	
	if is_dead:
		animation.play("Death")
		
	elif not is_hurt:
		movement()
	else:
		animation.play("Hurt")
	
	velocity = move_and_slide(velocity)

func movement():
	if player:
		var player_pos = player.position
		velocity = position.direction_to(player_pos) * speed
		if position.x > player_pos.x:
			animation.set_flip_h(true)
		else:
			animation.set_flip_h(false)
		
		animation.play("Run")
	else:
		animation.play("Idle")

func _on_PlayerDetect_body_entered(body):
	player = body
	player_node = body

func _on_PlayerDetect_body_exited(_body):
	player = null

func hurt(n):
	if not is_hurt:
		invinTimer.start()
		is_hurt = true
		health -= n
		if health <= 0:
			die()

func die():
	player_node.add_score(points)
	collision_mask = 0
	collision_layer = 0
	$PlayerCollision.queue_free()
	is_dead = true
	

func _on_invincibilityTimer_timeout():
	is_hurt = false

func _on_AnimatedSprite_animation_finished():
	if animation.animation == "Death":
		queue_free()
